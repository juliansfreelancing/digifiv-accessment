// cards are arrange in ascending order
export const types = ['diamonds', 'clubs', 'spades']
export const numbers = ['ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'jack', 'queen', 'king']
